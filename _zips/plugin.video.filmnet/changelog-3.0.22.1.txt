+ 3.0.21
- Fix resolver

+ 3.0.20
- Fix the NEWADDON Unknown Video Info Key Error

+ 3.0.19
- Fix TV Shows library issue.

+ 3.0.18
- Update cfscrape

+ 3.0.17
- Update scrapers

+ 3.0.16
- Update scrapers

+ 3.0.15
- Update scrapers

+ 3.0.14
- Fix Trakt Key/Api issue
- Change trakt api url using https

+ 3.0.13
- Fix Trakt authorization

+ 3.0.12
- Update scrapers
- Fix missing labels
- Fix Trakt authorization on Tools

+ 3.0.11
- Update scrapers
- Add new debrid scraper

+ 3.0.10
- Fix language missing labels
- Fix not show aired date

+ 3.0.9
- Remove Metahandler
- Add script.trakt and trakt context menu addons as requirements
- better experience for TRAKT users(rating, marking)

+ 3.0.8
- Better view of trakt progress episodes
- Add option for special episodes(default false)

+ 3.0.7
- Add new torrent scrapers
- Fix spanish scrapers
- Update english scrapers
- Fix kodi 18 craches caused on trailers play
- Fix settings.xml issues for kodi18

+ 3.0.6
- Fix strings.po issues

+ 3.0.5
- Update Greek scrapers
- Add HD Releases Movies

+ 3.0.4
- Update scrapers

+ 3.0.3
- Add Torrent scraper
- Update scrapers
- Remove scrapers

+ 3.0.2
- Add context menu on movies/episodes for checking subtitle availability

+ 3.0.1
- Update scrapers
- Fix rd infos issue

+ 3.0.0
- Update scrapers
- Add torrent scrapers
- Add torrent support(premium users only)

+ 2.9.6
- Update el scrapers

+ 2.9.5
- Update scrapers
- Fix SubzTV next page issue

+ 2.9.4
- Update scrapers
- Add Tools option in context menu

+ 2.9.3
- Update scrapers

+ 2.9.2
- Update scrapers
- Add Subztv.online on Movies/Tv Shows Menu

+ 2.9.1
- Update/Fix Greek scrapers

+ 2.9.0
- Update/Fix Greek scrapers
- Fix in Theaters movies issue

+ 2.8.9
- Update/Fix Greek scrapers
- Fix in Theaters movies issue

+ 2.8.8
- Update scrapers
- Fix in Theaters movies issue

+ 2.8.7
- Update scrapers

+ 2.8.6
- Update scrapers

+ 2.8.5
- Update scrapers
- add scrapers

+ 2.8.4
- Update scrapers

+ 2.8.3
- Update scrapers

+ 2.8.2
- Add scrapers
- Update scrapers
- Remove scrapers
- Clean up providers in settings.xml

+ 2.8.1
- Add scrapers
- Update scrapers
- Remove scrapers
- Clean up providers in settings.xml

+ 2.8.0
- Add scrapers
- Update scrapers
- Remove scrapers
- Clean up providers in settings.xml
- Add Enable/Disable all providers option

+ 2.7.3
- Update scrapers
- Remove scrapers

+ 2.7.2
- Update scrapers
- Update client
- Various cosmetics

+ 2.7.1
- Update scrapers

+ 2.7.0
- Addition of dual lines display option
- Added disclaimer & description into Greek language
- Various cosmetics

+ 2.6.6
- add scraper
- Various scrapers updates

+ 2.6.5
- add scrapers
- remove scrapers
- Various scrapers updates

+ 2.6.4
- add scrapers
- remove scrapers
- Various scrapers updates

+ 2.6.3
- add scrapers
- Various scrapers updates

+ 2.6.1
- fix search function for kodi 18
- fix year/episode regex
- Various scrapers updates

+ 2.6.0
- Add Furk.net scraper
- Various scrapers updates

+ 2.5.8
- Various scrapers updates

+ 2.5.7
- Various scrapers updates
- Updated quality regex

+ 2.5.6
- Added hosts pair context menu
- Language strings tweaks
- Scrapers updates
- Various cosmetics

+ 2.5.5
- Update scrapers
- Re-add movietoken
- Update quality regex

+ 2.5.4
- Updates/tweaks to scrapers
- Fix quality checker regex

+ 2.5.3
- Fix pagination for imdb lists

+ 2.5.2
- Updates/tweaks to scrapers

+ 2.5.1
- Updates/tweaks to scrapers
- Fix imdb next page error

+ 2.5.0
- Various updates/tweaks to scrapers
- Changed trakt/tvdb language code gr to 'el'
- Tweaks to direct stream tagging
- Added more streaminfo to labels
- Bumped metadata module version

+ 2.4.5
- Various updates to scrapers
- Tweaks to info labeling

+ 2.4.4
- Various updates to scrapers

+ 2.4.3
- Various updates to scrapers

+ 2.4.2
- Re-applied PEP8 into sources.py
- Update tzampa scraper

+ 2.4.1
- Fix no stream available error

+ 2.4.0
- Various updates/fixes to scrapers. Most notably fixed laggy operation
- Various PEP8
- Moved RD and some cache arguments into service module. Account details are checked every 12 hours.

+ 2.3.2
- Various updates to scrapers.
- imdb tvshow fix

+ 2.3.1
- Various updates to scrapers.
- Revert to enum setting for subtitles.

+ 2.3.0
- Various updates to scrapers.
- Created Real Debrid info tab in options + action in tools menu.
- Fixed imdb scraper
- Modules tweaks

+ 2.2.6
- Various updates to scrapers.
- Fix quality labeling.

+ 2.2.5
- Fix error in sources
- remove no working scrapers

+ 2.2.4
- Clean up and updates to provides/scrapers

+ 2.2.3
- Updates to scrapers
- Add Real-Debrid details

+ 2.2.2
- Updates to scrapers
- Various enhancements to modules

+ 2.2.1
- Some new, some update scrapers

+ 2.2.0
- Greek + English providers now as default
- Fixed imdb links for scrapers
- Added GiB in the size parameter

+ 2.1.8
- Updates to scrapers

+ 2.1.7
- Removed alluc
- Some new, some updated scrapers

+ 2.1.6
- Various updates to scrapers

+ 2.1.5
- Various updates to scrapers

+ 2.1.4
- Various updates to scrapers
- Minor change in formating for the pair tool menu item

+ 2.1.3
- New & updated scrapers

+ 2.1.2
- Added the options conditional for subtitles on PseudoTV's section
- Native method for detecting presense of subtitles is now used.
- Subtitles options for OS are disabled on preference that does not need them
- Added notification that lets user be aware that the sources streams' pulling has started
- Re-work of settings: A-Z Sort of providers, reversed visibility equilizers, hid fanart api key
- A few scraper updates

+ 2.1.1
- Fixed autosubs behaviour when dialog or auto type
- Added parenthesis to ensure autosubs conditional works properly